﻿using WebAPI_CSharp.Model;
using AutoMapper;
using WebAPI_CSharp.DataTransferObject.DTOCharacter;

namespace WebAPI_CSharp.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, DTOReadCharacter>();
            CreateMap<DTOCreateCharacter, Character>();
            CreateMap<DTOEditCharacter, Character>();
        }
    }
}
