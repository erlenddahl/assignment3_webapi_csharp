﻿using AutoMapper;
using WebAPI_CSharp.DataTransferObject.DTOFranchise;
using WebAPI_CSharp.Model;

namespace WebAPI_CSharp.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, DTOReadFranchise>();
            CreateMap<DTOCreateFranchise, Franchise>();
            CreateMap<DTOEditFranchise, Franchise>();
        }
    }
}
