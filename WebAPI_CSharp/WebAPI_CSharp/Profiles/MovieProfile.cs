﻿using WebAPI_CSharp.Model;
using AutoMapper;
using WebAPI_CSharp.DataTransferObject.DTOMovie;

namespace WebAPI_CSharp.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, DTOReadMovie>();
            CreateMap<DTOCreateMovie, Movie>();
            CreateMap<DTOEditMovie, Movie>();
        }
    }
}
