﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI_CSharp.DataTransferObject.DTOCharacter;
using WebAPI_CSharp.Model;
using WebAPI_CSharp.Services;

namespace WebAPI_CSharp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterService _service;
        private readonly IMapper _mapper;
        public CharacterController(ICharacterService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /**<summary>
         * Get all character records
         * </summary>
         * <returns>List of Characters from DTOReadCharacter</returns>*/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOReadCharacter>>> GetCharacters()
        {
            var characterModel = await _service.GetCharactersAsync();
            return _mapper.Map<List<DTOReadCharacter>>(characterModel);
        }

        /**<summary>
         * Get a character by id
         * </summary>
         * <returns>Character from DTOReadCharacter</returns>*/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOReadCharacter>> GetCharacter(int id)
        {
            var characterModel = await _service.GetCharacterAsync(id);

            if (characterModel == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTOReadCharacter>(characterModel);
        }

        /**<summary>
         * Update a character by id
         * </summary>
         * <returns>204NoContent if succesfull</returns>*/
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // PUT: api/Characters/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, DTOEditCharacter characterDto)
        {
            var characterModel = _mapper.Map<Character>(characterDto);
            if (id != characterModel.Id)
            {
                return BadRequest();
            }

            try
            {
                await _service.UpdateCharacterAsync(characterModel);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_service.CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /**<summary>
        * Add a new character
        * </summary>
        * <returns>CreatedAtActionResult</returns>*/
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // POST: api/Characters
        [HttpPost]
        public async Task<ActionResult<DTOReadCharacter>> PostCharacter(DTOCreateCharacter characterDto)
        {
            var characterModel = _mapper.Map<Character>(characterDto);
            await _service.AddCharacterAsync(characterModel);

            return CreatedAtAction("GetCharacter", new { id = characterModel.Id }, characterDto);
        }

        /**<summary>
         * Delete a character by id
         * </summary>
         * <returns>204NoContent if succesfull</returns>*/
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_service.CharacterExists(id))
            {
                return NotFound();
            }
            await _service.DeleteCharacterAsync(id);

            return NoContent();
        }
    }
}
