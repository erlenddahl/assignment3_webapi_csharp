﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI_CSharp.DataTransferObject.DTOCharacter;
using WebAPI_CSharp.DataTransferObject.DTOMovie;
using WebAPI_CSharp.Model;
using WebAPI_CSharp.Services;

namespace WebAPI_CSharp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly IMovieService _service;
        private readonly IMapper _mapper;

        public MovieController(IMovieService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /**<summary>
         * Get all movie records
         * </summary>
         * <returns>List of Movies from DTOReadMovie</returns>*/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOReadMovie>>> GetMovies()
        {
            var movieModel = await _service.GetMoviesAsync();
            return _mapper.Map<List<DTOReadMovie>>(movieModel);
        }

        /**<summary>
         * Get a movie by id
         * </summary>
         * <returns>Movie from DTOReadMovie</returns>*/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOReadMovie>> GetMovie(int id)
        {
            var movieModel = await _service.GetMovieAsync(id);

            if (movieModel == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTOReadMovie>(movieModel);
        }

        /**<summary>
         * Get all characters from a movie by id
         * </summary>
         * <returns>List of Characters from DTOReadCharacter</returns>*/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Movie/GetMovieCharacters/5
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<DTOReadCharacter>>> GetMovieCharacters(int id)
        {
            if (!_service.MovieExists(id))
            {
                return NotFound();
            }
            var characterModel = await _service.GetMovieCharactersAsync(id);

            return _mapper.Map<List<DTOReadCharacter>>(characterModel);
        }

        /**<summary>
         * Update a movie by id
         * </summary>
         * <returns>204NoContent if succesfull</returns>*/
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // PUT: api/Movies/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, DTOEditMovie movieDto)
        {
            var movieModel = _mapper.Map<Movie>(movieDto);
            if (id != movieModel.Id)
            {
                return BadRequest();
            }

            try
            {
                await _service.UpdateMovieAsync(movieModel);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_service.MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /**<summary>
         * Update which characters are in a movie
         * </summary>
         * <returns>204NoContent if succesfull</returns>*/
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // PUT: api/Movies/5/characterIds
        [HttpPut("{id}/characterIds")]
        public async Task<IActionResult> PutCharactersInMovie(int id, List<int> characterIds)
        {
            if (!_service.MovieExists(id))
            {
                return NotFound();
            }
            await _service.UpdateCharactersInMovieAsync(id, characterIds);

            return NoContent();
        }

        /**<summary>
        * Add a new movie
        * </summary>
        * <returns>CreatedAtActionResult</returns>*/
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // POST: api/Movies
        [HttpPost]
        public async Task<ActionResult<DTOReadMovie>> PostMovie(DTOCreateMovie movieDto)
        {
            var movieModel = _mapper.Map<Movie>(movieDto);
            await _service.AddMovieAsync(movieModel);

            return CreatedAtAction("GetMovie", new { id = movieModel.Id }, movieDto);
        }

        /**<summary>
         * Delete a movie by id
         * </summary>
         * <returns>204NoContent if succesfull</returns>*/
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_service.MovieExists(id))
            {
                return NotFound();
            }
            await _service.DeleteMovieAsync(id);

            return NoContent();
        }
    }
}
