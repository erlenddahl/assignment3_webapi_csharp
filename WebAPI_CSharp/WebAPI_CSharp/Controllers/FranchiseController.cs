﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI_CSharp.DataTransferObject.DTOCharacter;
using WebAPI_CSharp.DataTransferObject.DTOFranchise;
using WebAPI_CSharp.DataTransferObject.DTOMovie;
using WebAPI_CSharp.Model;
using WebAPI_CSharp.Services;

namespace WebAPI_CSharp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchiseController : ControllerBase
    {
        private readonly IFranchiseService _service;
        private readonly IMapper _mapper;

        public FranchiseController(IFranchiseService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /**<summary>
         * Get all franchise records
         * </summary>
         * <returns>List of Franchises from DTOReadFranchise</returns>*/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Franchise
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOReadFranchise>>> GetFranchises()
        {
            var franchiseModel = await _service.GetFranchisesAsync();
            return _mapper.Map<List<DTOReadFranchise>>(franchiseModel);
        }

        /**<summary>
         * Get a franchise by id
         * </summary>
         * <returns>Franchise from DTOReadFranchise</returns>*/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Franchise/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOReadFranchise>> GetFranchise(int id)
        {
            var franchiseModel = await _service.GetFranchiseAsync(id);

            if (franchiseModel == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTOReadFranchise>(franchiseModel);
        }

        /**<summary>
         * Get all movies from a franchise by id
         * </summary>
         * <returns>List of Movies from DTOReadMovie</returns>*/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Franchise/GetFranchiseMovies/5
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<DTOReadMovie>>> GetFranchiseMovies(int id)
        {
            var movieModel = await _service.GetFranchiseMoviesAsync(id);
            if (movieModel == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<DTOReadMovie>>(movieModel);
        }

        /**<summary>
         * Get all characters from a franchise by id
         * </summary>
         * <returns>List of Characters from DTOReadCharacter</returns>*/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Franchise/GetFranchiseCharacters/5
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<IEnumerable<DTOReadCharacter>>> GetFranchiseCharacters(int id)
        {

            if (!_service.FranchiseExists(id))
            {
                return NotFound();
            }
            var characterModel = await _service.GetFranchiseCharactersAsync(id);

            return _mapper.Map<List<DTOReadCharacter>>(characterModel);
        }

        /**<summary>
         * Update a franchise by id
         * </summary>
         * <returns>204NoContent if succesfull</returns>*/
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // PUT: api/Franchise/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, DTOEditFranchise franchiseDto)
        {
            var franchiseModel = _mapper.Map<Franchise>(franchiseDto);
            if (id != franchiseModel.Id)
            {
                return BadRequest();
            }
            try
            {
                await _service.UpdateFranchiseAsync(franchiseModel);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_service.FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /**<summary>
         * Update which movies are in a franchise
         * </summary>
         * <returns>204NoContent if succesfull</returns>*/
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // PUT: api/Franchise/5/movieIds
        [HttpPut("{id}/movieIds")]
        public async Task<IActionResult> PutMoviesInFranchise(int id, List<int> movieIds)
        {
            if (!_service.FranchiseExists(id))
            {
                return NotFound();
            }
            await _service.UpdateMoviesInFranchise(id, movieIds);

            return NoContent();
        }

        /**<summary>
        * Add a new franchise
        * </summary>
        * <returns>CreatedAtActionResult</returns>*/
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // POST: api/Franchise
        [HttpPost]
        public async Task<ActionResult<DTOReadFranchise>> PostFranchise(DTOCreateFranchise franchiseDto)
        {
            var franchiseModel = _mapper.Map<Franchise>(franchiseDto);
            await _service.AddFranchiseAsync(franchiseModel);

            return CreatedAtAction("GetFranchise", new { id = franchiseModel.Id }, franchiseDto);
        }

        /**<summary>
         * Delete a franchise by id
         * </summary>
         * <returns>204NoContent if succesfull</returns>*/
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // DELETE: api/Franchise/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_service.FranchiseExists(id))
            {
                return NotFound();
            }
            await _service.DeleteFranchiseAsync(id);

            return NoContent();
        }
    }
}
