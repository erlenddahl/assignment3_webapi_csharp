﻿namespace WebAPI_CSharp.DataTransferObject.DTOFranchise
{
    public class DTOReadFranchise
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string? Description { get; set; }
    }
}
