﻿namespace WebAPI_CSharp.DataTransferObject.DTOFranchise
{
    public class DTOCreateFranchise
    {
        public string Name { get; set; }
        public string? Description { get; set; }
    }
}
