﻿using Microsoft.EntityFrameworkCore;
using WebAPI_CSharp.Data;
using WebAPI_CSharp.Model;

namespace WebAPI_CSharp.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;

        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Franchise>> GetFranchisesAsync()
        {
            return await _context.Franchises.ToListAsync();

        }

        public async Task<Franchise> GetFranchiseAsync(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }

        public async Task<IEnumerable<Movie>> GetFranchiseMoviesAsync(int id)
        {
            return await _context.Movies.Where(m => m.FranchiseId == id).ToListAsync();
        }

        public async Task<IEnumerable<Character>> GetFranchiseCharactersAsync(int id)
        {
            var movieModel = await _context.Movies.Where(m => m.FranchiseId == id).ToListAsync();
            return await _context.Characters.Where(c => c.Movies.Any(ci => movieModel.Contains(ci))).ToListAsync();
        }

        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMoviesInFranchise(int id, List<int> movieIds)
        {
            Franchise franchise = await _context.Franchises.FindAsync(id);
            List<Movie> movies = new List<Movie>();
            foreach (int movieId in movieIds)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                movies.Add(movie);
            }
            franchise.Movies = movies;

            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
