﻿using System.Collections.Generic;
using WebAPI_CSharp.Model;

namespace WebAPI_CSharp.Services
{
    public interface ICharacterService
    {
        /**<summary>
         * Get all character records from db
         * </summary>
         * <returns>List of characters</returns>*/
        public Task<IEnumerable<Character>> GetCharactersAsync();

        /**<summary>
         * Get a character by id from db
         * </summary>
         * <returns>Character</returns>*/
        public Task<Character> GetCharacterAsync(int id);

        /**<summary>
         * Update a character by id in db
         * </summary>*/
        public Task UpdateCharacterAsync(Character character);

        /**<summary>
        * Add a new character in db
        * </summary>*/
        public Task AddCharacterAsync(Character character);

        /**<summary>
        * Delete character from db
        * </summary>*/
        public Task DeleteCharacterAsync(int id);

        /**<summary>
        * See if character with id exists in db
        * </summary>
        * <returns>True or False</returns>*/
        public bool CharacterExists(int id);
    }
}
