﻿using WebAPI_CSharp.Model;

namespace WebAPI_CSharp.Services
{
    public interface IMovieService
    {
        /**<summary>
         * Get all movie records from db
         * </summary>
         * <returns>List of movies</returns>*/
        public Task<IEnumerable<Movie>> GetMoviesAsync();

        /**<summary>
         * Get a movie by id from db
         * </summary>
         * <returns>Movie</returns>*/
        public Task<Movie> GetMovieAsync(int id);

        /**<summary>
         * Get all characters from a movie by id
         * </summary>
         * <returns>List of Characters</returns>*/
        public Task<IEnumerable<Character>> GetMovieCharactersAsync(int id);

        /**<summary>
         * Update a movie by id in db
         * </summary>*/
        public Task UpdateMovieAsync(Movie movie);

        /**<summary>
         * Update which characters are in a movie
         * </summary>*/
        public Task UpdateCharactersInMovieAsync(int id, List<int> characterIds);

        /**<summary>
        * Add a new movie in db
        * </summary>*/
        public Task AddMovieAsync(Movie movie);

        /**<summary>
        * Delete movie from db
        * </summary>*/
        public Task DeleteMovieAsync(int id);

        /**<summary>
       * See if movie with id exists in db
       * </summary>
       * <returns>True or False</returns>*/
        public bool MovieExists(int id);
    }
}
