﻿using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography.Xml;
using WebAPI_CSharp.Data;
using WebAPI_CSharp.Model;

namespace WebAPI_CSharp.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieDbContext _context;

        public MovieService(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Movie>> GetMoviesAsync()
        {
            return await _context.Movies.ToListAsync();

        }

        public async Task<Movie> GetMovieAsync(int id)
        {
            return await _context.Movies.FindAsync(id);
        }

        public async Task<IEnumerable<Character>> GetMovieCharactersAsync(int id)
        {
            var movieModel = await _context.Movies.FindAsync(id);
            return await _context.Characters.Where(c => c.Movies.Contains(movieModel)).ToListAsync();
        } 

        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateCharactersInMovieAsync(int id, List<int> characterIds)
        {
            Movie movieModel = await _context.Movies.FindAsync(id);
            List<Character> characters = new List<Character>();
            foreach (int characterId in characterIds)
            {
                Character characterModel = await _context.Characters.FindAsync(characterId);
                characters.Add(characterModel);
            }
            movieModel.Characters = characters;
            _context.Entry(movieModel).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
