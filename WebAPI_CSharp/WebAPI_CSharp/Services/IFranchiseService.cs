﻿using WebAPI_CSharp.Model;

namespace WebAPI_CSharp.Services
{
    public interface IFranchiseService
    {
        /**<summary>
         * Get all franchise records from db
         * </summary>
         * <returns>List of franchises</returns>*/
        public Task<IEnumerable<Franchise>> GetFranchisesAsync();

        /**<summary>
         * Get a franchise by id from db
         * </summary>
         * <returns>Franchise</returns>*/
        public Task<Franchise> GetFranchiseAsync(int id);

        /**<summary>
         * Get all movies from a franchise by id
         * </summary>
         * <returns>List of Movies</returns>*/
        public Task<IEnumerable<Movie>> GetFranchiseMoviesAsync(int id);

        /**<summary>
         * Get all characters from a franchise by id
         * </summary>
         * <returns>List of Characters</returns>*/
        public Task<IEnumerable<Character>> GetFranchiseCharactersAsync(int id);

        /**<summary>
         * Update a franchise by id in db
         * </summary>*/
        public Task UpdateFranchiseAsync(Franchise franchise);

        /**<summary>
         * Update which movies are in a franchise
         * </summary>*/
        public Task UpdateMoviesInFranchise(int id, List<int> movieIds);

        /**<summary>
        * Add a new franchise in db
        * </summary>*/
        public Task AddFranchiseAsync(Franchise franchise);

        /**<summary>
        * Delete franchise from db
        * </summary>*/
        public Task DeleteFranchiseAsync(int id);

        /**<summary>
        * See if franchise with id exists in db
        * </summary>
        * <returns>True or False</returns>*/
        public bool FranchiseExists(int id);
    }
}
