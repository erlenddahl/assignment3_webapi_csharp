﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebAPI_CSharp.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "varchar(20)", nullable: false),
                    Alias = table.Column<string>(type: "varchar(20)", nullable: true),
                    Gender = table.Column<string>(type: "varchar(10)", nullable: true),
                    Picture = table.Column<string>(type: "varchar(MAX)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "varchar(40)", nullable: false),
                    Description = table.Column<string>(type: "varchar(MAX)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(type: "varchar(100)", nullable: false),
                    Genre = table.Column<string>(type: "varchar(100)", nullable: true),
                    ReleaseYear = table.Column<int>(type: "int", nullable: true),
                    Director = table.Column<string>(type: "varchar(20)", nullable: false),
                    Picture = table.Column<string>(type: "varchar(MAX)", nullable: true),
                    Trailer = table.Column<string>(type: "varchar(MAX)", nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    CharacterId = table.Column<int>(type: "int", nullable: false),
                    MovieId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.CharacterId, x.MovieId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, null, "Legolas Greenleaf", "Male", "https://static.wikia.nocookie.net/lotr/images/3/33/Legolas_-_in_Two_Towers.PNG/revision/latest?cb=20120916035151" },
                    { 2, "Olorin", "Gandalf", "Male", "https://static.wikia.nocookie.net/lotr/images/e/e7/Gandalf_the_Grey.jpg/revision/latest?cb=20121110131754" },
                    { 3, "Gollum", "Smeagol", "Male", "https://upload.wikimedia.org/wikipedia/en/e/e0/Gollum.PNG" },
                    { 4, "", "Obi-Wan Kenobi", "Male", "https://upload.wikimedia.org/wikipedia/en/c/c5/Obiwan1.jpg" },
                    { 5, "", "Yoda", "Male", "https://upload.wikimedia.org/wikipedia/commons/8/8a/Dereck_Hard_Yoda_-_Little_Reality_2016_%28cropped%29.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel The Lord of the Rings by J. R. R. Tolkien.", "The Lord of the Rings" },
                    { 2, "Star Wars is an American epic space opera multimedia franchise.", "Star Wars" },
                    { 3, "Ogre on adventures.", "Shrek" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "MovieTitle", "Picture", "ReleaseYear", "Trailer" },
                values: new object[,]
                {
                    { 1, "Peter Jackson", 1, "Action, Adventure, Drama", "The Lord of the Rings: The Fellowship of the Ring", "https://moviesmedia.ign.com/movies/image/object/033/033771/lotr_the-fellowship-of-the-ring_poster.jpg?width=300", 2001, "https://www.youtube.com/watch?v=V75dMMIW2B4&themeRefresh=1" },
                    { 2, "Peter Jackson", 1, "Action, Adventure, Drama", "The Lord of the Rings: The Two Towers", "https://static.wikia.nocookie.net/filmguide/images/f/f2/Lord_of_the_rings_the_two_towers_2002_intl_original_film_art_2000x.jpg/revision/latest?cb=20190813215951", 2002, "https://www.youtube.com/watch?v=hYcw5ksV8YQ" },
                    { 3, "George Lucas", 2, "Action, Adventure, Fantasy", "Star Wars: Episode I – The Phantom Menace", "https://www.imdb.com/title/tt0120915/mediaviewer/rm2234464000/?ref_=tt_ov_i", 1999, "https://www.imdb.com/video/vi2143788569/?playlistId=tt0120915&ref_=tt_ov_vi" },
                    { 4, "Goerge Lucas", 2, "Action, Adventure, Fantasy", "Star Wars: Episode II - Attack of the Clones", "https://www.imdb.com/title/tt0121765/mediaviewer/rm423895808/?ref_=tt_ov_i", 2002, "https://www.imdb.com/video/vi1132511001/?playlistId=tt0121765&ref_=tt_pr_ov_vi" },
                    { 5, "Andrew Adamson", 3, "Animation, Adventure, Comedy", "Shrek", "https://www.imdb.com/title/tt0126029/mediaviewer/rm955136512/?ref_=tt_ov_i", 2001, "https://www.imdb.com/video/vi4085711129/?playlistId=tt0126029&ref_=tt_pr_ov_vi" }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 2, 1 },
                    { 2, 2 },
                    { 3, 1 },
                    { 3, 2 },
                    { 4, 3 },
                    { 4, 4 },
                    { 5, 3 },
                    { 5, 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_MovieId",
                table: "CharacterMovie",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
