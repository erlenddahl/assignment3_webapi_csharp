﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WebAPI_CSharp.Data;

#nullable disable

namespace WebAPI_CSharp.Migrations
{
    [DbContext(typeof(MovieDbContext))]
    [Migration("20230203101559_InitialDb")]
    partial class InitialDb
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.13")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.Property<int>("CharacterId")
                        .HasColumnType("int");

                    b.Property<int>("MovieId")
                        .HasColumnType("int");

                    b.HasKey("CharacterId", "MovieId");

                    b.HasIndex("MovieId");

                    b.ToTable("CharacterMovie");

                    b.HasData(
                        new
                        {
                            CharacterId = 1,
                            MovieId = 1
                        },
                        new
                        {
                            CharacterId = 1,
                            MovieId = 2
                        },
                        new
                        {
                            CharacterId = 2,
                            MovieId = 1
                        },
                        new
                        {
                            CharacterId = 2,
                            MovieId = 2
                        },
                        new
                        {
                            CharacterId = 3,
                            MovieId = 1
                        },
                        new
                        {
                            CharacterId = 3,
                            MovieId = 2
                        },
                        new
                        {
                            CharacterId = 4,
                            MovieId = 3
                        },
                        new
                        {
                            CharacterId = 5,
                            MovieId = 3
                        },
                        new
                        {
                            CharacterId = 4,
                            MovieId = 4
                        },
                        new
                        {
                            CharacterId = 5,
                            MovieId = 4
                        });
                });

            modelBuilder.Entity("WebAPI_CSharp.Model.Character", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Alias")
                        .HasColumnType("varchar(20)");

                    b.Property<string>("FullName")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("Gender")
                        .HasColumnType("varchar(10)");

                    b.Property<string>("Picture")
                        .HasColumnType("varchar(MAX)");

                    b.HasKey("Id");

                    b.ToTable("Characters");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            FullName = "Legolas Greenleaf",
                            Gender = "Male",
                            Picture = "https://static.wikia.nocookie.net/lotr/images/3/33/Legolas_-_in_Two_Towers.PNG/revision/latest?cb=20120916035151"
                        },
                        new
                        {
                            Id = 2,
                            Alias = "Olorin",
                            FullName = "Gandalf",
                            Gender = "Male",
                            Picture = "https://static.wikia.nocookie.net/lotr/images/e/e7/Gandalf_the_Grey.jpg/revision/latest?cb=20121110131754"
                        },
                        new
                        {
                            Id = 3,
                            Alias = "Gollum",
                            FullName = "Smeagol",
                            Gender = "Male",
                            Picture = "https://upload.wikimedia.org/wikipedia/en/e/e0/Gollum.PNG"
                        },
                        new
                        {
                            Id = 4,
                            Alias = "",
                            FullName = "Obi-Wan Kenobi",
                            Gender = "Male",
                            Picture = "https://upload.wikimedia.org/wikipedia/en/c/c5/Obiwan1.jpg"
                        },
                        new
                        {
                            Id = 5,
                            Alias = "",
                            FullName = "Yoda",
                            Gender = "Male",
                            Picture = "https://upload.wikimedia.org/wikipedia/commons/8/8a/Dereck_Hard_Yoda_-_Little_Reality_2016_%28cropped%29.jpg"
                        });
                });

            modelBuilder.Entity("WebAPI_CSharp.Model.Franchise", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Description")
                        .HasColumnType("varchar(MAX)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("varchar(40)");

                    b.HasKey("Id");

                    b.ToTable("Franchises");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel The Lord of the Rings by J. R. R. Tolkien.",
                            Name = "The Lord of the Rings"
                        },
                        new
                        {
                            Id = 2,
                            Description = "Star Wars is an American epic space opera multimedia franchise.",
                            Name = "Star Wars"
                        },
                        new
                        {
                            Id = 3,
                            Description = "Ogre on adventures.",
                            Name = "Shrek"
                        });
                });

            modelBuilder.Entity("WebAPI_CSharp.Model.Movie", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Director")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<int?>("FranchiseId")
                        .HasColumnType("int");

                    b.Property<string>("Genre")
                        .HasColumnType("varchar(100)");

                    b.Property<string>("MovieTitle")
                        .IsRequired()
                        .HasColumnType("varchar(100)");

                    b.Property<string>("Picture")
                        .HasColumnType("varchar(MAX)");

                    b.Property<int?>("ReleaseYear")
                        .HasColumnType("int");

                    b.Property<string>("Trailer")
                        .HasColumnType("varchar(MAX)");

                    b.HasKey("Id");

                    b.HasIndex("FranchiseId");

                    b.ToTable("Movies");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Director = "Peter Jackson",
                            FranchiseId = 1,
                            Genre = "Action, Adventure, Drama",
                            MovieTitle = "The Lord of the Rings: The Fellowship of the Ring",
                            Picture = "https://moviesmedia.ign.com/movies/image/object/033/033771/lotr_the-fellowship-of-the-ring_poster.jpg?width=300",
                            ReleaseYear = 2001,
                            Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4&themeRefresh=1"
                        },
                        new
                        {
                            Id = 2,
                            Director = "Peter Jackson",
                            FranchiseId = 1,
                            Genre = "Action, Adventure, Drama",
                            MovieTitle = "The Lord of the Rings: The Two Towers",
                            Picture = "https://static.wikia.nocookie.net/filmguide/images/f/f2/Lord_of_the_rings_the_two_towers_2002_intl_original_film_art_2000x.jpg/revision/latest?cb=20190813215951",
                            ReleaseYear = 2002,
                            Trailer = "https://www.youtube.com/watch?v=hYcw5ksV8YQ"
                        },
                        new
                        {
                            Id = 3,
                            Director = "George Lucas",
                            FranchiseId = 2,
                            Genre = "Action, Adventure, Fantasy",
                            MovieTitle = "Star Wars: Episode I – The Phantom Menace",
                            Picture = "https://www.imdb.com/title/tt0120915/mediaviewer/rm2234464000/?ref_=tt_ov_i",
                            ReleaseYear = 1999,
                            Trailer = "https://www.imdb.com/video/vi2143788569/?playlistId=tt0120915&ref_=tt_ov_vi"
                        },
                        new
                        {
                            Id = 4,
                            Director = "Goerge Lucas",
                            FranchiseId = 2,
                            Genre = "Action, Adventure, Fantasy",
                            MovieTitle = "Star Wars: Episode II - Attack of the Clones",
                            Picture = "https://www.imdb.com/title/tt0121765/mediaviewer/rm423895808/?ref_=tt_ov_i",
                            ReleaseYear = 2002,
                            Trailer = "https://www.imdb.com/video/vi1132511001/?playlistId=tt0121765&ref_=tt_pr_ov_vi"
                        },
                        new
                        {
                            Id = 5,
                            Director = "Andrew Adamson",
                            FranchiseId = 3,
                            Genre = "Animation, Adventure, Comedy",
                            MovieTitle = "Shrek",
                            Picture = "https://www.imdb.com/title/tt0126029/mediaviewer/rm955136512/?ref_=tt_ov_i",
                            ReleaseYear = 2001,
                            Trailer = "https://www.imdb.com/video/vi4085711129/?playlistId=tt0126029&ref_=tt_pr_ov_vi"
                        });
                });

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.HasOne("WebAPI_CSharp.Model.Character", null)
                        .WithMany()
                        .HasForeignKey("CharacterId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("WebAPI_CSharp.Model.Movie", null)
                        .WithMany()
                        .HasForeignKey("MovieId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("WebAPI_CSharp.Model.Movie", b =>
                {
                    b.HasOne("WebAPI_CSharp.Model.Franchise", "Franchise")
                        .WithMany("Movies")
                        .HasForeignKey("FranchiseId")
                        .OnDelete(DeleteBehavior.SetNull);

                    b.Navigation("Franchise");
                });

            modelBuilder.Entity("WebAPI_CSharp.Model.Franchise", b =>
                {
                    b.Navigation("Movies");
                });
#pragma warning restore 612, 618
        }
    }
}
