﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace WebAPI_CSharp.Model
{
    public class Character
    {
        public int Id { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string FullName  { get; set; }
        [Column(TypeName = "varchar(20)"), AllowNull]
        public string? Alias { get; set; }
        [Column(TypeName = "varchar(10)")]
        public string? Gender { get; set; }
        [Column(TypeName = "varchar(MAX)"), AllowNull]
        public string? Picture { get; set; }
        public ICollection<Movie>? Movies { get; set; }
    }
}
