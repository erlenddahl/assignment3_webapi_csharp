﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI_CSharp.Model
{
    public class Franchise
    {
        public int Id { get; set; }
        [Column(TypeName = "varchar(40)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string? Description { get; set; }
        public ICollection<Movie>? Movies { get; set; }
    }
}
