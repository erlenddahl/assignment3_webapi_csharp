﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI_CSharp.Model
{
    public class Movie
    {
        public int Id { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string MovieTitle { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string? Genre { get; set; }
        public int? ReleaseYear { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string Director { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string? Picture { get; set; }
        [Column(TypeName = "varchar(MAX)")]
        public string? Trailer { get; set; }
        public ICollection<Character>? Characters { get; set; }
        [Column()]
        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }    
    }
}
