﻿using WebAPI_CSharp.Model;

namespace WebAPI_CSharp.Data
{
    public class SeedDataHelper
    {
        /**<summary>
         * Get a list of premade characters
         * </summary>
         * <returns>List of characters</returns>*/
        public static List<Character> GetCharacters()
        {
            List<Character> characters = new List<Character>()
            {
                new Character()
                {
                    Id = 1,
                    FullName = "Legolas Greenleaf",
                    Gender = "Male",
                    Picture = "https://static.wikia.nocookie.net/lotr/images/3/33/Legolas_-_in_Two_Towers.PNG/revision/latest?cb=20120916035151"
                },
                new Character()
                {
                    Id = 2,
                    FullName = "Gandalf",
                    Alias = "Olorin",
                    Gender = "Male",
                    Picture = "https://static.wikia.nocookie.net/lotr/images/e/e7/Gandalf_the_Grey.jpg/revision/latest?cb=20121110131754"
                },
                new Character()
                {
                    Id = 3,
                    FullName = "Smeagol",
                    Alias = "Gollum",
                    Gender = "Male",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/e/e0/Gollum.PNG"
                },
                new Character()
                {
                    Id = 4,
                    FullName = "Obi-Wan Kenobi",
                    Alias = "",
                    Gender = "Male",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/c/c5/Obiwan1.jpg"
                },
                new Character()
                {
                    Id = 5,
                    FullName = "Yoda",
                    Alias = "",
                    Gender = "Male",
                    Picture = "https://upload.wikimedia.org/wikipedia/commons/8/8a/Dereck_Hard_Yoda_-_Little_Reality_2016_%28cropped%29.jpg"
                }
            };
            return characters;
        }

        /**<summary>
         * Get a list of premade movies
         * </summary>
         * <returns>List of movies</returns>*/
        public static List<Movie> GetMovies()
        {
            List<Movie> movies = new List<Movie>()
            {
                new Movie()
                {
                    Id = 1,
                    MovieTitle = "The Lord of the Rings: The Fellowship of the Ring",
                    Genre = "Action, Adventure, Drama",
                    ReleaseYear = 2001,
                    Director = "Peter Jackson",
                    Picture = "https://moviesmedia.ign.com/movies/image/object/033/033771/lotr_the-fellowship-of-the-ring_poster.jpg?width=300",
                    Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4&themeRefresh=1",
                    FranchiseId = 1
                },
                new Movie()
                {
                    Id = 2,
                    MovieTitle = "The Lord of the Rings: The Two Towers",
                    Genre = "Action, Adventure, Drama",
                    ReleaseYear = 2002,
                    Director = "Peter Jackson",
                    Picture = "https://static.wikia.nocookie.net/filmguide/images/f/f2/Lord_of_the_rings_the_two_towers_2002_intl_original_film_art_2000x.jpg/revision/latest?cb=20190813215951",
                    Trailer = "https://www.youtube.com/watch?v=hYcw5ksV8YQ",
                    FranchiseId = 1
                },
                new Movie()
                {
                    Id = 3,
                    MovieTitle = "Star Wars: Episode I – The Phantom Menace",
                    Genre = "Action, Adventure, Fantasy",
                    ReleaseYear = 1999,
                    Director = "George Lucas",
                    Picture = "https://www.imdb.com/title/tt0120915/mediaviewer/rm2234464000/?ref_=tt_ov_i",
                    Trailer = "https://www.imdb.com/video/vi2143788569/?playlistId=tt0120915&ref_=tt_ov_vi",
                    FranchiseId = 2
                },
                new Movie()
                {
                    Id = 4,
                    MovieTitle = "Star Wars: Episode II - Attack of the Clones",
                    Genre = "Action, Adventure, Fantasy",
                    ReleaseYear = 2002,
                    Director = "Goerge Lucas",
                    Picture = "https://www.imdb.com/title/tt0121765/mediaviewer/rm423895808/?ref_=tt_ov_i",
                    Trailer = "https://www.imdb.com/video/vi1132511001/?playlistId=tt0121765&ref_=tt_pr_ov_vi",
                    FranchiseId = 2
                },
                new Movie()
                {
                    Id = 5,
                    MovieTitle = "Shrek",
                    Genre = "Animation, Adventure, Comedy",
                    ReleaseYear = 2001,
                    Director = "Andrew Adamson",
                    Picture = "https://www.imdb.com/title/tt0126029/mediaviewer/rm955136512/?ref_=tt_ov_i",
                    Trailer = "https://www.imdb.com/video/vi4085711129/?playlistId=tt0126029&ref_=tt_pr_ov_vi",
                    FranchiseId = 3
                }
            };
            return movies;
        }
        /**<summary>
         * Get a list of premade franchises
         * </summary>
         * <returns>List of franchises</returns>*/

        public static List<Franchise> GetFranchises()
        {
            List<Franchise> franchises = new List<Franchise>()
            {
                new Franchise()
                {
                    Id = 1,
                    Name = "The Lord of the Rings",
                    Description = "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel The Lord of the Rings by J. R. R. Tolkien."
                },
                new Franchise()
                {
                    Id = 2,
                    Name = "Star Wars",
                    Description = "Star Wars is an American epic space opera multimedia franchise."
                },
                new Franchise()
                {
                    Id = 3,
                    Name = "Shrek",
                    Description = "Ogre on adventures."
                }
            };
            return franchises;
        }
    }
}
