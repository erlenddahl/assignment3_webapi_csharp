﻿using Microsoft.EntityFrameworkCore;
using WebAPI_CSharp.Model;

namespace WebAPI_CSharp.Data
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Character>().HasData(SeedDataHelper.GetCharacters());
            modelBuilder.Entity<Movie>().HasData(SeedDataHelper.GetMovies());
            modelBuilder.Entity<Franchise>().HasData(SeedDataHelper.GetFranchises());

            modelBuilder.Entity<Character>().HasMany(m => m.Movies).WithMany(c => c.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    ch => ch.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    mo => mo.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    lt =>
                    {
                        lt.HasKey("CharacterId", "MovieId");
                        lt.HasData(
                            new { CharacterId = 1, MovieId = 1},
                            new { CharacterId = 1, MovieId = 2},
                            new { CharacterId = 2, MovieId = 1},
                            new { CharacterId = 2, MovieId = 2},
                            new { CharacterId = 3, MovieId = 1},
                            new { CharacterId = 3, MovieId = 2},
                            new { CharacterId = 4, MovieId = 3},
                            new { CharacterId = 5, MovieId = 3},
                            new { CharacterId = 4, MovieId = 4},
                            new { CharacterId = 5, MovieId = 4}

                        );
                    });
            modelBuilder.Entity<Movie>()
               .HasOne<Franchise>(m => m.Franchise)
               .WithMany(f => f.Movies)
               .HasForeignKey(m => m.FranchiseId)
               .IsRequired(false)
               .OnDelete(DeleteBehavior.SetNull);
        }

        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
    }
}
