# Assignment3_WebAPI_CSharp

In this project we created a database with Entity Framework using the code first approach. Then we set up a Web API using different concepts like controllers, data transfer objects and services so that the database is accsessible through http queries. To run the project you have to set your server address in appsettings.json at Data Source.

# Contributors
Erlend Halsne Dahl (@Erlend-Halsne-Dahl) An Binh Nguyen (@anbinhnguy)
